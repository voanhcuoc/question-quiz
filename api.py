from sqlalchemy.orm import sessionmaker

from models import Quiz, engine

Session = sessionmaker(bind=engine)

def create_quiz(record):
    session = Session()
    new = Quiz(
        question = record.get('question'),
        answer = record.get('answer')
    )
    session.add(new)
    session.commit()
    id = new.id
    session.close()
    return id

def read_quiz(id):
    session = Session()
    quiz = session.query(Quiz).filter_by(id=id).first()
    return quiz

def all_quiz():
    session = Session()
    quizzes = session.query(Quiz).all()
    session.close()
    return quizzes

def update_quiz(id, record):
    session = Session()
    quiz = session.query(Quiz).filter_by(id=id).first()
    question = record.get('question')
    answer = record.get('answer')
    if question:
        quiz.question = question
    if answer:
        quiz.answer = answer
    session.commit()
    session.close()

def delete_quiz(id):
    session = Session()
    quiz = session.query(Quiz).filter_by(id=id).first()
    quiz.delete()
    session.commit()
    session.close()
