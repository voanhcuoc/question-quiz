from sqlalchemy import Column, String, Integer, create_engine
from sqlalchemy.ext.declarative import declarative_base

Model = declarative_base();

class Quiz(Model):
    __tablename__ = 'quiz'
    id = Column(Integer, primary_key=True)
    question = Column(String)
    answer = Column(String)

engine = create_engine('sqlite://')
Model.metadata.create_all(engine)
