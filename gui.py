from random import sample
import tkinter as tk
from serial import Serial

import mock
from api import all_quiz

font = ('Times', 30)

class App(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.block = True
        self.config(width=500, height=500)
        self.pack(fill=None)
        self.create_question_label()
        self.create_answer_labels()
        self.create_next_button()
        all_quizzes = all_quiz()
        self.quizzes = iter(sample(all_quizzes, 4))

        self.next_quiz()

        self.amountTrue = 0
        self.amountFalse = 0

    def create_question_label(self):
        space = tk.Label(self)
        space.config(height=5)
        space.pack(side='top')
        self.question = tk.Label(self)
        self.question.config(width = 50, height = 5, relief=tk.RIDGE, font=font)
        self.question.config(background = '#D5EAFF')
        self.question.pack(side='top')

    def create_answer_labels(self):
        self.yes = tk.Label(self)
        self.yes.config(text='yes', width = 8, height = 2, font=font)
        self.yes.pack(side='left')
        self.no = tk.Label(self)
        self.no.config(text='no', width = 8, height = 2, font=font)
        self.no.pack(side='right')

    def create_next_button(self):
        self.next = tk.Button(self)
        self.next.config(text='Next')
        self.next.config(command=self.next_quiz)
        self.next.config(width = 5, height = 3)
        credit = tk.Label(self)
        credit.config(height = 3, text='Mr Ninh dear Mr Tu', font=('Times', 20))
        credit.pack(side='bottom')
        self.next.pack(side='bottom')
        space = tk.Label(self)
        space.config(height = 15)
        space.pack()

    def next_quiz(self):
        try:
            quiz = next(self.quizzes)
            self.current_quiz = quiz
            self.question.config(text = quiz.question)
            self.yes.config(background='white')
            self.no.config(background='white')
            self.block = False
            print('> refresh')
        except StopIteration:
            result = 'true: {}, false: {}'.format(self.amountTrue, self.amountFalse)
            self.question.config(text=result)
            self.next.config(text='return')
            self.next.config(command=self.reset)

    def reset(self):
        self.pack_forget()
        self.__init__(self.master)

serial = Serial('/dev/ttyUSB0') # TODO detect
print(serial.name)
serial.timeout = 0.01

root = tk.Tk()
root.minsize(width=500, height=500)
app = App(master=root)
def serial_loop():
    raw = serial.readline()
    signal = str(raw, encoding='utf-8').strip()
    print(repr(signal))
    print(app.block)
    if signal and not app.block:
        if signal == app.current_quiz.answer:
            serial.write(b'T')
            app.block = True
            app.amountTrue += 1
            getattr(app, signal).config(background='green')
        else:
            serial.write(b'F')
            app.block = True
            app.amountFalse += 1
            getattr(app, signal).config(background='red')
    root.after(100, serial_loop)

root.after(100, serial_loop)
app.mainloop()
