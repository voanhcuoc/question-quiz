import csv

from api import create_quiz
from models import Quiz, engine

# Quiz.__table__.delete()

with open('data.csv') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for (question, answer) in reader:
        record = dict(question=question, answer=answer)
        create_quiz(record)
